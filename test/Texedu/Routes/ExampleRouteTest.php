<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 15.03.2018
 * Time: 14:38
 */

namespace Texedu\Routes;


use FrameStack\Testing\Routing\MockRequest;
use FrameStack\Testing\Routing\MockResponse;
use PHPUnit\Framework\TestCase;
use Texedu\App\App;

class ExampleRouteTest extends TestCase
{

    private $app;
    private $request;
    private $response;

    /**
     * @dataProvider nameProvider
     */
    public function testHello($name)
    {

        $this->request = new MockRequest("GET", [], [], ['name' => $name]);
        $this->response = new MockResponse();

        $route = new ExampleRoute();
        $route->hello(new App(), $this->request, $this->response);

        $this->assertContains($this->request->getUrlParams()->name('name'), $this->response->getPayload(), 'Payload contains teststring');
    }

    public function nameProvider() {
        return [
            ['Jens'],
            ['Can'],
            ['Michigrind'],
            ['Tschuldigung'],
            ['-1'],
        ];
    }

}