<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 09.03.2018
 * Time: 17:07
 */

require_once 'vendor/autoload.php';

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Tools\Setup;


$paths = array( realpath(__DIR__."/../src/Texedu/Entities") );
$isDevMode = true;

$connection = [
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . '/../db.sqlite',
];

$cache = new \Doctrine\Common\Cache\ArrayCache();


$reader = new AnnotationReader();
$driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($reader, $paths);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$config->setMetadataCacheImpl($cache);
$config->setQueryCacheImpl($cache);
$config->setMetadataDriverImpl($driver);


$em = \Doctrine\ORM\EntityManager::create($connection, $config);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);