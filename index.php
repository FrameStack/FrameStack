<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 22.02.2018
 * Time: 13:47
 */
require_once('vendor/autoload.php');

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\ApcuCache;
use Doctrine\ORM\Tools\Setup;
use FrameStack\Routing\Dispatchers\FastRoute\CachedFastRouteDispatcher;
use FrameStack\Routing\RouteProviders\Annotations\AnnotationRouteProvider;

$DEV_MODE = true;

$app = new \Texedu\App\App();
$app->persistentCache = new \Doctrine\Common\Cache\PhpFileCache(__DIR__ . '/cache');


$paths = array( realpath(__DIR__."/src/Texedu/Entities") );
$isDevMode = true;

$connection = [
    'driver' => 'pdo_sqlite',
    'path' => __DIR__ . '/db.sqlite',
];

$cache = new \Doctrine\Common\Cache\ArrayCache();


$reader = new AnnotationReader();
$driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($reader, $paths);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$config->setMetadataCacheImpl($cache);
$config->setQueryCacheImpl($cache);
$config->setMetadataDriverImpl($driver);


$app->em = \Doctrine\ORM\EntityManager::create($connection, $config);
$app->log = new \FrameStack\Log\PHPErrorLogger();

$app->auth = new \FrameStack\Auth\DoctrineSessionAuthProvider($app->em, "_AUTH", \Texedu\Entities\User::class);

$manager = new CachedFastRouteDispatcher(
    new AnnotationRouteProvider(__DIR__ . "/src/Texedu"),
    new \Doctrine\Common\Cache\ArrayCache(),
    1);

$uri = $_SERVER['REQUEST_URI'];

error_log('New request!');

$response = new \Texedu\App\Response();
$response->setHeader('Access-Control-Allow-Origin', '*');
$response->setHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
$response->setHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

/** @var \Texedu\App\Response $response */
$response = $manager->dispatch($app, \Texedu\App\Request::getHTTPRequest(), $response);
$response->sendHeaders();
echo $response->getPayload();
