<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 25.02.2018
 * Time: 18:05
 */

namespace Texedu\App;


class Response
{

    protected $headers = [];
    protected $payload;

    protected $type;

    public function setHeader($name, $value) {
        $this->headers[$name] = $value;

        return $this;
    }

    public function sendHeaders() {
        foreach ($this->headers as $name => $value) {
            error_log("Header: ${name}: ${value}");
            header($name . ": " . $value);
        }

        return $this;
    }

    public function setHeaderContentType($value) {
        $this->setHeader('Content-Type', $value);
        return $this;
    }

    public function code(int $code) {
        http_response_code($code);
        return $this;
    }

    public function json(array $data) {
        $this->setHeaderContentType('application/json');
        $this->payload = json_encode($data);
        return $this;
    }

    public function text($data) {
        $this->setHeaderContentType('text/plain');
        $this->payload = $data;

        return $this;
    }

    public function html($data) {
        $this->payload = $data;
        return $this;
    }

    public function nothing() {
        header('Content-Type: text/html');
        header_remove('Content-Type');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayload()
    {
        return $this->payload;
    }

}