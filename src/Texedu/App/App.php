<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 25.02.2018
 * Time: 18:06
 */

namespace Texedu\App;


use Doctrine\Common\Cache\Cache;
use Doctrine\ORM\EntityManager;
use FrameStack\Auth\AuthProvider;
use Psr\Log\LoggerInterface;

class App
{

    /**
     * @var Cache
     */
    public $persistentCache;

    /**
     * @var AuthProvider
     */
    public $auth;

    /**
     * @var LoggerInterface
     */
    public $log;

    /**
     * @var EntityManager
     */
    public $em;

}