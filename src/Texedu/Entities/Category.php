<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 11.03.2018
 * Time: 15:05
 */

namespace Texedu\Entities;
use Doctrine\ORM\Mapping as ORM;
use FrameStack\Util\FS_Serializable;

/**
 * Class Category
 * @package Texedu\Entities
 * @ORM\Entity
 * @ORM\Table(name="category")
 */
class Category implements FS_Serializable
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", length=11, type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", length=255, type="string")
     */
    private $name;

    /**
     * @var Category
     *
     * @ORM\Column(name="fk_parent", length=11, type="integer", nullable=true)
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     */
    private $parent;

    /**
     * @var Category[]
     *
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     */
    private $children;

    /**
     * @var Document[]
     *
     * @ORM\OneToMany(targetEntity="Document", mappedBy="category")
     */
    private $documents;

    public function serialize(): array {

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'parent' => $this->getParent() ? $this->getParent()->getId() : null,
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): Category
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Category $parent
     * @return Category
     */
    public function setParent($parent): Category
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Category[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param Category[] $children
     * @return Category
     */
    public function setChildren(array $children): Category
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return Document[]
     */
    public function getDocuments(): array
    {
        return $this->documents;
    }

    /**
     * @param Document[] $documents
     * @return Category
     */
    public function setDocuments(array $documents): Category
    {
        $this->documents = $documents;
        return $this;
    }

}