<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 11.03.2018
 * Time: 15:11
 */

namespace Texedu\Entities;
use Doctrine\ORM\Mapping as ORM;
use FrameStack\Util\FS_Serializable;


/**
 * Class Document
 * @package Texedu\Entities
 *
 * @ORM\Entity
 * @ORM\Table(name="document")
 */
class Document implements FS_Serializable
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", length=11, type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", length=255, type="string")
     */
    private $name;

    /**
     * @var Category
     *
     * @ORM\Column(name="category", type="integer", length=11)
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="documents")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="content_source", type="text")
     */
    private $contentSource;

    /**
     * @var string
     *
     * @ORM\Column(name="content_html", type="text", nullable=true)
     */
    private $contentHTML;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Document
     */
    public function setName(string $name): Document
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Document
     */
    public function setCategory(Category $category): Document
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentSource(): string
    {
        return $this->contentSource;
    }

    /**
     * @param string $contentSource
     * @return Document
     */
    public function setContentSource(string $contentSource): Document
    {
        $this->contentSource = $contentSource;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentHTML(): string
    {
        return $this->contentHTML;
    }

    /**
     * @param string $contentHTML
     * @return Document
     */
    public function setContentHTML(string $contentHTML): Document
    {
        $this->contentHTML = $contentHTML;
        return $this;
    }

    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'category' => $this->getCategory()->getId(),
        ];
    }
}