<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 09.03.2018
 * Time: 16:31
 */

namespace Texedu\Entities;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class User
 * @package Texedu\Entities
 *
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", length=11, type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", length=255, type="string")
     */
    private $username;


    /**
     * @var string
     *
     * @ORM\Column(name="password", length=255, type="string", nullable=true)
     */
    private $password;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

}