<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 09.03.2018
 * Time: 16:07
 */

namespace Texedu\Routes;


use FrameStack\Routing\RouteProviders\Annotations\Annotations\Route;
use Texedu\App\App;
use Texedu\App\Request;
use Texedu\App\Response;
use Texedu\Entities\User;

class Authentication
{

    /**
     * @param App $app
     * @param Request $request
     * @param Response $response
     *
     * @Route(method="POST", url="/auth/login")
     * @return Response
     */
    public function login(App $app, Request $request, Response $response)
    {
        $data = $request->getJSONPost();
        if ($app->auth->login($data['username'], $data['password'])) {
            return $response->json([
                'success' => true,
            ]);
        } else {
            return $response->json([
                'success' => false,
            ])->code(401);
        }
    }

    /**
     * @param App $app
     * @param Request $request
     * @param Response $response
     *
     * @Route(method="POST", url="/auth/gen")
     * @return Response
     */
    public function generatePassword(App $app, Request $request, Response $response) {
        return $response->json([
            'input' => $request->getJSONPost()['input'],
            'output' => password_hash($request->getJSONPost()['input'], PASSWORD_DEFAULT),
        ]);
    }

    /**
     * @Route(method="POST", url="/auth/createAdmin")
     */
    public function createAdmin(App $app, Request $request, Response $response) {

        if ($app->em->getRepository(User::class)->count([])) {
            $app->log->warning("Tried to create an Admin User when there already is one");

            return $response->json([
                'success' => false,
                'reason' => 'A user already exists',
            ])->code(401);
        } else {
            $user = new User();
            $user->setUsername($request->getJSONPost()['username']);
            $user->setPassword(password_hash($request->getJSONPost()['password'], PASSWORD_DEFAULT));

            $app->em->persist($user);
            $app->em->flush();

            return $response->json([
                'success' => true,
            ]);
        }
    }

}