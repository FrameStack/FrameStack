<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 11.03.2018
 * Time: 15:38
 */

namespace Texedu\Routes;
use FrameStack\Routing\RouteProviders\Annotations\Annotations\Route;
use FrameStack\Routing\RouteProviders\Annotations\Annotations\RouteGroup;
use FrameStack\Util\FS_SerializableUtil;
use Texedu\App\App;
use Texedu\App\Request;
use Texedu\App\Response;
use Texedu\Entities\Category;
use Texedu\Entities\Document;

/**
 * Class CategoryRoute
 * @package Texedu\Routes
 *
 * @RouteGroup(url="/category")
 */
class CategoryRoute
{

    /**
     * @Route(method="GET", url="/list")
     */
    public function getCategories(App $app, Request $request, Response $response) {

        $responseData = [];

        $root = $request->getJSONPost()['root'];
        if (!$root) {
            $root = null;
            $responseData['documents'] = [];
        } else {
            $documents = $app->em->getRepository(Document::class)->findBy(['category' => $root]);
            $responseData['documents'] = FS_SerializableUtil::serializeList($documents);
        }

        $categories = $app->em->getRepository(Category::class)->findBy(['parent' => null]);

        $responseData['categories'] = FS_SerializableUtil::serializeList($categories);

        return $response->json($responseData);
    }

    /**
     * @Route(method="POST", url="/create")
     */
    public function createCategory(App $app, Request $request, Response $response) {

        $requestData = $request->getJSONPost();

        $parent = null;
        if ($requestData['parent']) {
            /** @var Category $parent */
            $parent = $app->em->getRepository(Category::class)->find($requestData['parent']);
        }

        $category = new Category();
        $category->setParent($parent);
        $category->setName($requestData['name']);

        $app->em->persist($category);
        $app->em->flush();

        return $response->json(['success' => true]);
    }

}