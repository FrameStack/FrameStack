<?php
/**
 * Created by IntelliJ IDEA.
 * User: jvogler
 * Date: 13.03.2018
 * Time: 20:45
 */

namespace Texedu\Routes;

use FrameStack\Routing\RouteProviders\Annotations\Annotations\RouteGroup;
use FrameStack\Routing\RouteProviders\Annotations\Annotations\Route;
use FrameStack\Util\FS_Serializable;
use FrameStack\Util\FS_SerializableUtil;
use Texedu\App\App;
use Texedu\App\Request;
use Texedu\App\RequestRepository;
use Texedu\App\Response;
use Texedu\Entities\Document;


/**
 * Class ExampleRoute
 *
 * @RouteGroup(url="/example")
 * @package Texedu\Routes
 */
class ExampleRoute {

    /**
     * URL Parameters are defined by {<name>:<regex>}
     * The regex part is optional to match anything
     *
     * The annotation can be Ctrl+Clicked or Ctrl+B for a lookup of the annotation definition
     * same goes for the params
     *
     * @Route(method="GET", url="/hello/{name:\w+}")
     */
    public function hello(App $app, Request $request, Response $response) {
        $name = $request->getUrlParams()->name('name');
        return $response->html("<h1>Welcome ${name}!</h1>");
    }

    /**
     * @Route(method="GET", url="/list")
     */
    public function list(App $app, Request $request, Response $response) {
        $data = $app->em->getRepository(Document::class)->findAll();
        return $response->json(FS_SerializableUtil::serializeList($data));
    }

    /**
     * @Route(method="POST", url="/create")
     */
    public function create(App $app, Request $request, Response $response) {

        $rData = $request->getJSONPost();

        $doc = new Document();
        $doc->setName($rData->name('name'));
        $doc->setContentHTML($rData->string('content'));

        $app->em->persist($doc);
        $app->em->flush();

        return $response->json(['success' => true])->code(203);
    }

    /**
     * @Route(method="DELETE", url="/delete")
     */
    public function delete(App $app, Request $request, Response $response) {
        $rData = $request->getJSONPost();
        $document = $app->em->getRepository(Document::class)->find($rData->int('id'));
        if($document instanceof Document) {
            $app->em->remove($document);
            $app->em->flush();

            return $response->json(['success' => true]);
        } else {
            return $response
                ->json(['success' => false, 'message' => 'Document ID does not exist'])
                ->code(404);
        }

    }


    /**
     * @Route(method="GET", url="/form")
     */
    public function showForm(App $app, Request $request, Response $response) {
        $form = new ExampleForm();
        return $response->renderTemplate('example/form', [
            'form' => $form,
        ]);
    }

    /**
     * Form example (no Form implementation available)
     *
     * @Route(method="POST", url="/form/submit")
     */
    public function submitForm(App $app, Request $request, Response $response) {
        $form = new ExampleForm();
        $form->fetchInput($request->getPost());

        if ($form->validate()) {
            $user = new User();
            $user->setUsername($form->username);
            $user->setPassword($form->password);
            $user->setEmail($form->email);

            $app->em->persist($user);
            $app->em->flush();

            return $response->redirect('/user/profile');
        } else {
            return $response->html('BAD!<br>' . $form->getValidationErrors())->code(500);
        }
    }


}
