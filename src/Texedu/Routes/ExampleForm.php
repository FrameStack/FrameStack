<?php
/**
 * Created by IntelliJ IDEA.
 * User: jvogler
 * Date: 13.03.2018
 * Time: 21:13
 */

namespace Texedu\Routes;


/**
 * @Form(baseName="example", jsValidation=true)
 */
class ExampleForm  {

    /**
     * @FormField(type="text", validator="name", minlength=6)
     */
    public $username;

    /**
     * @FormField(type="text", validator="email")
     */
    public $email;

    /**
     * @FormField(type="password", validator="passwordContraints", validatorParams={"complexity" = 20})
     */
    public $password;

    /**
     * @FormField(type="password", validator="confirmField", validatorParams={"referenceField" = "password"})
     */
    public $confirmPassword;

    /**
     * @FormField(type="checkbox", required=false)
     */
    public $newletterSubscription;
}
