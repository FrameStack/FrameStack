<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 11.03.2018
 * Time: 15:28
 */

namespace FrameStack\Log;

class PHPErrorLogger extends \Psr\Log\AbstractLogger
{

    public function log($level, $message, array $context = array())
    {
        error_log("[${level}] ${message}" . ($context ? "\n".print_r($context, true) : ""));
    }
}