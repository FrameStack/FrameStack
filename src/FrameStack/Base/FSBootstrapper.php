<?php
/**
 * Created by IntelliJ IDEA.
 * User: jvogler
 * Date: 12.03.2018
 * Time: 18:14
 */

namespace FrameStack\Base;


interface FSBootstrapper {

    public static function bootstrapDev();

    public static function bootstrapProd();

}
