<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 11.03.2018
 * Time: 15:45
 */

namespace FrameStack\Util;


class FS_SerializableUtil
{

    /**
     * @param FS_Serializable[] $list
     * @return array
     */
    public static function serializeList($list): array {

        $data = [];

        foreach ($list as $item) {
            $data[] = $item->serialize();
        }

        return $data;
    }

}