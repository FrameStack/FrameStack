<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 11.03.2018
 * Time: 15:43
 */

namespace FrameStack\Util;

interface FS_Serializable
{
    public function serialize(): array;

}