<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 23.02.2018
 * Time: 10:51
 */

namespace FrameStack\Routing;


class GroupInfo
{

    /**
     * @var string
     */
    public $url;

    /**
     * @var RouteInfo[]|GroupInfo[]
     */
    public $routes;

}