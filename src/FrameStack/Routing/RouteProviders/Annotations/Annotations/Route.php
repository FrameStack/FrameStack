<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 22.02.2018
 * Time: 13:51
 */

namespace FrameStack\Routing\RouteProviders\Annotations\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Defines a method which can be called by a HTTP Request
 *
 * @package JVogler\AnnoRouter\Annotations
 * @Annotation
 * @Target("METHOD")
 */
class Route
{

    /**
     * @var string One of the HTTP Request methods (GET,POST,DELETE, etc.)
     */
    public $method;

    /**
     * @var string The actual url including optional parameters
     */
    public $url;

}