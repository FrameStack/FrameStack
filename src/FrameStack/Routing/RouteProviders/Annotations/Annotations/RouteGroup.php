<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 23.02.2018
 * Time: 10:50
 */

namespace FrameStack\Routing\RouteProviders\Annotations\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\Annotation\Target;

/**
 * Class RouteGroup
 * @package JVogler\AnnoRouter\Annotations
 * @Annotation
 * @Target("CLASS")
 */
class RouteGroup
{

    public $url;

}