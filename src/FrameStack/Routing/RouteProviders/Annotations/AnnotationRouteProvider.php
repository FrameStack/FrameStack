<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 23.02.2018
 * Time: 09:01
 */

namespace FrameStack\Routing\RouteProviders\Annotations;


use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use FrameStack\Routing\GroupInfo;
use FrameStack\Routing\RouteInfo;
use FrameStack\Routing\RouteProviders\Annotations\Annotations\Route;
use FrameStack\Routing\RouteProviders\Annotations\Annotations\RouteGroup;
use FrameStack\Routing\RouteProviders\RouteProvider;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;

class AnnotationRouteProvider implements RouteProvider
{

    /**
     * @var array List of Annotations to be loaded for the AnnotationReader
     */
    private static $annotationClasses = [
        Route::class,
        RouteGroup::class
    ];

    /**
     * @var bool True if the annotations already have been loaded
     */
    private static $annotationsLoaded = false;

    /**
     * @var string Path to look for routes
     */
    private $path;

    /**
     * @var AnnotationReader
     */
    private $reader;

    /**
     * Loads all routes in the specified path by looking at their annotations
     *
     * @param $path
     */
    public function __construct($path)
    {
        self::loadAnnotations();
        $this->path = $path;
        try {
            $annotationParser = new \Doctrine\Common\Annotations\DocParser();
            $annotationParser->setIgnoreNotImportedAnnotations(true);
            $this->reader = new AnnotationReader($annotationParser);
        } catch (AnnotationException $e) {
            throw new \RuntimeException("Should not happen", 0, $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function loadRoutes()
    {

        /** @var RouteInfo[] $routes */
        $routes = [];

        foreach (self::loadRouteFiles($this->path) as $routeClassName) {
            $routeClass = new \ReflectionClass($routeClassName);

            $currentRoutes =& $routes;

            foreach ($this->reader->getClassAnnotations($routeClass) as $annotation) {
                if ($annotation instanceof RouteGroup) {
                    $groupInfo = new GroupInfo();
                    $groupInfo->url = $annotation->url;
                    $groupInfo->routes = [];
                    $currentRoutes =& $groupInfo->routes;
                    $routes[] = $groupInfo;
                    error_log("New Group: " . $routeClass->getName());
                }
            }

            foreach ($routeClass->getMethods() as $method) {


                foreach ($this->reader->getMethodAnnotations($method) as $annotation) {
                    if ($annotation instanceof Route) {
                        $currentRoutes[] = RouteInfo::createFromAnnotation(
                            $annotation,
                            $routeClass->getFileName(),
                            $routeClassName,
                            $method->getName()
                        );

                        error_log("New Route: " . $method->getName());
                    }
                }

            }

        }

        return $routes;
    }

    /**
     * Requires all annotation classes to accommodate the {@link AnnotationReader}
     */
    private static function loadAnnotations()
    {
        if (self::$annotationsLoaded) {
            return;
        }

        AnnotationRegistry::registerLoader('class_exists');
        self::$annotationsLoaded = true;

        return;

        /* In case the deprecation never gets resolved:
        foreach (self::$annotationClasses as $annotationClass) {
            $clz = new \ReflectionClass($annotationClass);
            require_once $clz->getFileName();
        }

        */
    }


    private static function loadRouteFiles($path)
    {
        $fqcns = [];

        $allFiles = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
        $phpFiles = new RegexIterator($allFiles, '/\.php$/');
        foreach ($phpFiles as $phpFile) {
            $phpFilePath = $phpFile->getRealPath();
            $content = file_get_contents($phpFile->getRealPath());
            $tokens = token_get_all($content);
            $namespace = '';
            //print_r($tokens);
            for ($index = 0; isset($tokens[$index]); $index++) {
                if (!isset($tokens[$index][0])) {
                    continue;
                }
                if (T_NAMESPACE === $tokens[$index][0]) {
                    $index += 2; // Skip namespace keyword and whitespace
                    while (isset($tokens[$index]) && is_array($tokens[$index])) {
                        $namespace .= $tokens[$index++][1];
                    }
                }
                if (T_CLASS === $tokens[$index][0]) {

                    // Ignore Classname::class
                    if (isset($tokens[$index - 1][0]) && $tokens[$index - 1][0] == T_DOUBLE_COLON) {
                        continue;
                    }

                    $index += 2; // Skip class keyword and whitespace

                    if (!is_array($tokens[$index]) || $tokens[$index][0] != T_STRING) {
                        continue;
                    }

                    $fqcns[$phpFilePath] = $namespace . '\\' . $tokens[$index][1];
                }
            }
        }

        foreach ($fqcns as $file => $clz) {
            /** @noinspection PhpIncludeInspection */
            require_once $file;
        }

        return $fqcns;
    }

}