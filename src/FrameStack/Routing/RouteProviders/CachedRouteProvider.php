<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 23.02.2018
 * Time: 10:18
 */

namespace FrameStack\Routing\RouteProviders;


use Doctrine\Common\Cache\CacheProvider;
use FrameStack\Routing\RouteInfo;

class CachedRouteProvider implements RouteProvider
{

    const CACHE_KEY = "RouteProvider_Cache";

    private $actualProvider;
    private $cacheProvider;
    private $ttl = 0;

    public function __construct(RouteProvider $actualProvider, CacheProvider $cacheProvider, $ttl = 0)
    {
        $this->actualProvider = $actualProvider;
        $this->cacheProvider = $cacheProvider;
        $this->ttl = $ttl;
    }

    /**
     * Loads all routing info
     *
     * @return RouteInfo[]
     */
    public function loadRoutes()
    {
        if ($this->cacheProvider->contains(self::CACHE_KEY)) {
            return $this->cacheProvider->fetch(self::CACHE_KEY);
        } else {
            $routes = $this->actualProvider->loadRoutes();
            $this->cacheProvider->save(self::CACHE_KEY, $routes, $this->ttl);
            return $routes;
        }
    }
}