<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 23.02.2018
 * Time: 08:58
 */

namespace FrameStack\Routing\RouteProviders;


use FrameStack\Routing\RouteInfo;

interface RouteProvider
{

    /**
     * Loads all routing info
     *
     * @return RouteInfo[]
     */
    public function loadRoutes();

}