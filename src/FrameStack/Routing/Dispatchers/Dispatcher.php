<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 23.02.2018
 * Time: 09:26
 */

namespace FrameStack\Routing\Dispatchers;


use DI\Container;
use FrameStack\Routing\Exceptions\MethodNotAllowedException;
use FrameStack\Routing\Exceptions\RouteNotFoundException;
use Texedu\App\Request;
use Texedu\App\Response;

interface Dispatcher
{

    /**
     * @param Container $container
     * @param Request $request
     * @param Response $response
     * @return mixed
     * @throws RouteNotFoundException
     * @throws MethodNotAllowedException
     */
    public function dispatch(Container $container, Request $request, Response $response);

}