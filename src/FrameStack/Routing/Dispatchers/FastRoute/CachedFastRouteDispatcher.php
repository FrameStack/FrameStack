<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 23.02.2018
 * Time: 12:16
 */

namespace FrameStack\Routing\Dispatchers\FastRoute;


use Doctrine\Common\Cache\CacheProvider;
use FrameStack\Routing\RouteProviders\RouteProvider;

class CachedFastRouteDispatcher extends FastRouteDispatcher
{
    const CACHE_KEY = 'FastRouteDispatcherData';

    /**
     * @var CacheProvider
     */
    private $cacheProvider;

    /**
     * @var int
     */
    private $ttl;

    public function __construct(RouteProvider $provider, CacheProvider $cacheProvider, $ttl = 0)
    {
        $this->cacheProvider = $cacheProvider;
        $this->ttl = $ttl;

        parent::__construct($provider);
    }

    protected function getRoutingData()
    {
        if ($this->cacheProvider->contains(self::CACHE_KEY)) {
            return $this->cacheProvider->fetch(self::CACHE_KEY);
        } else {
            $data = parent::getRoutingData();
            $this->cacheProvider->save(self::CACHE_KEY, $data, $this->ttl);
            return $data;
        }
    }


}