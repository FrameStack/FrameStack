<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 22.02.2018
 * Time: 13:48
 */

namespace FrameStack\Routing\Dispatchers\FastRoute;


use DI\Container;
use FastRoute\Dispatcher as FRDispatcher;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteCollector;
use FrameStack\Routing\Dispatchers\Dispatcher;
use FrameStack\Routing\Exceptions\MethodNotAllowedException;
use FrameStack\Routing\Exceptions\RouteNotFoundException;
use FrameStack\Routing\GroupInfo;
use FrameStack\Routing\RouteInfo;
use FrameStack\Routing\RouteProviders\RouteProvider;
use Texedu\App\Request;
use Texedu\App\Response;

class FastRouteDispatcher implements Dispatcher
{

    /**
     * @var RouteInfo[]
     */
    private $routes = [];

    /**
     * @var FRDispatcher
     */
    private $dispatcher;

    public function __construct(RouteProvider $provider)
    {
        $this->routes = $provider->loadRoutes();
        $this->dispatcher = new GroupCountBased($this->getRoutingData());
    }

    /**
     */
    protected function getRoutingData()
    {
        $parser = new \FastRoute\RouteParser\Std();
        $dataGenerator = new \FastRoute\DataGenerator\GroupCountBased();
        $routeCollector = new RouteCollector($parser, $dataGenerator);
        $this->collectRoutes($routeCollector);

        return $routeCollector->getData();
    }

    private function collectRoutes(RouteCollector $r, $routes = null)
    {
        if ($routes == null) {
            $routes = $this->routes;
        }

        foreach ($routes as $route) {
            if ($route instanceof RouteInfo) {
                $r->addRoute($route->httpMethod, $route->url, $route);
            } else if ($route instanceof GroupInfo) {
                $r->addGroup($route->url, function (RouteCollector $r) use ($route) {
                    $this->collectRoutes($r, $route->routes);
                });
            }
        }

    }

    /**
     * @param Container $container
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \DI\DependencyException
     * @throws \ReflectionException
     * @throws RouteNotFoundException
     * @throws MethodNotAllowedException
     */
    public function dispatch(Container $container, Request $request, Response $response)
    {
        if ($request->getMethod() == 'OPTIONS') {
            return $response->nothing();
        }

        $dispatchInfo = $this->dispatcher->dispatch($request->getMethod(), $request->getUri());

        //print_r($dispatchInfo);

        if ($dispatchInfo[0] == FRDispatcher::NOT_FOUND) {
            throw new RouteNotFoundException($request->getUri());
        } else if ($dispatchInfo[0] == FRDispatcher::METHOD_NOT_ALLOWED) {
            throw new MethodNotAllowedException($request->getUri(), $request->getMethod());
        }

        $request->setUrlParams($dispatchInfo[2]);

        /** @var RouteInfo $routeInfo */
        $routeInfo = $dispatchInfo[1];
        require_once $routeInfo->filename;

        $reflectionClass = new \ReflectionClass($routeInfo->classFQN);
        $reflectionMethod = $reflectionClass->getMethod($routeInfo->methodName);

        $instance = $reflectionClass->newInstance();
        $container->injectOn($instance);
        return $reflectionMethod->invoke($instance, $request, $response);
    }

}