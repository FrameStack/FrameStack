<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 23.02.2018
 * Time: 08:58
 */

namespace FrameStack\Routing;


use FrameStack\Routing\RouteProviders\Annotations\Annotations\Route;

class RouteInfo
{

    /**
     * @var string HTTP Method (GET, POST, etc)
     */
    public $httpMethod;

    /**
     * @var string URL
     */
    public $url;

    /**
     * @var string Filename of the class which contains to method
     * Used to load the file if it hasn't been loaded yet
     */
    public $filename;

    /**
     * @var string Class containing the method
     */
    public $classFQN;

    /**
     * @var string Method name
     */
    public $methodName;

    /**
     * @param Route $route The route annotation
     * @param string $filename The path of the file containing the method
     * @param string $classFQN FQN Of the class containing the method
     * @param string $methodName The actual name of the method
     * @return RouteInfo
     */
    public static function createFromAnnotation(Route $route, $filename, $classFQN, $methodName)
    {
        $obj = new RouteInfo();
        $obj->httpMethod = $route->method;
        $obj->url = $route->url;

        $obj->filename = $filename;
        $obj->classFQN = $classFQN;
        $obj->methodName = $methodName;

        return $obj;
    }

}