<?php
/**
 * Created by IntelliJ IDEA.
 * User: jvogler
 * Date: 13.03.2018
 * Time: 20:43
 */

namespace FrameStack\Routing;


use Texedu\App\App;
use Texedu\App\Request;
use Texedu\App\Response;

interface IRouteClass {

    public function preHandler(App $app, Request $request, Response $response): Response;

    public function postHandler(App $app, Request $request, Response $response): Response;

}
