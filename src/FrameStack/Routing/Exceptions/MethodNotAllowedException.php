<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 05.10.2018
 * Time: 17:14
 */

namespace FrameStack\Routing\Exceptions;


use Throwable;

class MethodNotAllowedException extends \Exception
{
    public function __construct(string $uri, string $method = "", int $code = 405, Throwable $previous = null)
    {
        parent::__construct("Method not allowed: {$method} on {$uri}", $code, $previous);
    }


}