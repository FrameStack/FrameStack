<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 05.10.2018
 * Time: 17:11
 */

namespace FrameStack\Routing\Exceptions;


use Throwable;

class RouteNotFoundException extends \Exception
{
    public function __construct(string $route = "", int $code = 404, Throwable $previous = null)
    {
        parent::__construct("Route not found: " . $route, $code, $previous);
    }

}