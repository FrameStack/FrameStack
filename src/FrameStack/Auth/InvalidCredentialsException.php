<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 09.03.2018
 * Time: 16:19
 */

namespace FrameStack\Auth;


use Throwable;

class InvalidCredentialsException extends \Exception
{

    public function __construct(string $message = "Invalid username or password!", int $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}