<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 09.03.2018
 * Time: 16:04
 */

namespace FrameStack\Auth;


interface AuthUser
{

    public function getId(): int;

    public function getUsername(): string;

    public function getPassword(): string;

}