<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 09.03.2018
 * Time: 16:13
 */

namespace FrameStack\Auth;


use Doctrine\ORM\EntityManager;

class DoctrineSessionAuthProvider implements AuthProvider
{

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $sessionNamespace;

    /**
     * @var string
     */
    protected $userClz;

    public function __construct($entityManager, $sessionNamespace, $userClz)
    {
        $this->em = $entityManager;
        $this->sessionNamespace = $sessionNamespace;
        $this->userClz = $userClz;
    }

    protected function getSessionData(): array {
        if (isset($_SESSION[$this->sessionNamespace])) {
            return $_SESSION[$this->sessionNamespace];
        }
        return [];
    }

    protected function setSessionData(array $data): void {
        $_SESSION[$this->sessionNamespace] = $data;
    }

    public function isLoggedIn(): bool
    {
        return isset($this->getSessionData()['user_id']);
    }

    /**
     * {@inheritDoc}
     */
    public function login($username, $password): bool
    {
        /** @var AuthUser $userObj */
        $userObj = $this->em->getRepository($this->userClz)->findBy(['username' => $username]);

        if ($userObj instanceof AuthUser) {
            if (password_verify($password, $userObj->getPassword())) {
                return true;
            } else {
                return false;
            }
        } else {
            password_verify($password, "");
            return false;
        }
    }

    public function logout(): bool
    {
        $this->setSessionData([]);
        return true;
    }

    public function getCurrentUser(): AuthUser
    {
        /** @var AuthUser $user */
        $user = $this->em->getRepository($this->userClz)->find($this->getSessionData()['user_id']);
        return $user;
    }
}