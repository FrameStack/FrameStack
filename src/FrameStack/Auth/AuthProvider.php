<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 09.03.2018
 * Time: 15:58
 */

namespace FrameStack\Auth;


interface AuthProvider
{

    public function isLoggedIn(): bool;

    /**
     * @param $username
     * @param $password
     * @return bool
     */
    public function login($username, $password): bool;

    public function logout(): bool;

    public function getCurrentUser(): AuthUser;

}