<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 15.03.2018
 * Time: 14:44
 */

namespace FrameStack\Testing\Routing;


use Texedu\App\Response;

class MockResponse extends Response
{

    public function payloadContains(string $data, $message = '') {

        assertContains($data, $this->payload, $message);

    }

}