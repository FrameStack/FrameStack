<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 15.03.2018
 * Time: 14:40
 */

namespace FrameStack\Testing\Routing;


use Texedu\App\Request;
use Texedu\App\RequestRepository;

class MockRequest extends Request
{

    public function __construct(string $method, array $queryParams, array $postParams, array $urlParams)
    {
        parent::__construct($method, "MOCK_REQUEST");
        $this->queryParams = new RequestRepository($queryParams);
        $this->postParams = new RequestRepository($postParams);
        $this->urlParams = new RequestRepository($urlParams);
    }

}