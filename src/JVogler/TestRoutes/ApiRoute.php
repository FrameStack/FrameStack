<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 02.03.2018
 * Time: 14:31
 */

namespace JVogler\TestRoutes;


use FrameStack\Routing\RouteProviders\Annotations\Annotations\Route;
use FrameStack\Routing\RouteProviders\Annotations\Annotations\RouteGroup;
use Texedu\App\App;
use Texedu\App\Request;
use Texedu\App\Response;

/**
 * Class ApiRoute
 * @package JVogler\TestRoutes
 * @RouteGroup(url="/api")
 */
class ApiRoute
{

    const KEY_DOCUMENTS = '_texedu_docs';
    private $provider;

    public function __construct()
    {
    }

    /**
     * @param App $app
     * @param Request $request
     * @param Response $response
     * @Route(method="GET", url="/documents")
     */
    public function getDocuments(App $app, Request $request, Response $response)
    {
        $data = $app->persistentCache->fetch(self::KEY_DOCUMENTS);

        if ($data === FALSE) {
            return $response->json([]);
        }

        return $response->json($data);

    }

    /**
     * @param App $app
     * @param Request $request
     * @param Response $response
     * @Route(method="POST", url="/document/save")
     */
    public function saveDocument(App $app, Request $request, Response $response) {

        $data = (array)$request->getJSONPost();

        error_log(print_r($data, true));
        $entries = $app->persistentCache->fetch(self::KEY_DOCUMENTS);
        if ($entries === FALSE) {
            $entries = [];
        }

        $found = false;
        foreach ($entries as $key => $val) {
            if ($val['name'] == $data['name']) {
                $entries[$key] = $data;
                $found = true;
                break;
            }
        }

        if (!$found) {
            $entries[] = $data;
        }

        $app->persistentCache->save(self::KEY_DOCUMENTS, $entries);

        return $response->json(["success" => "true"]);
    }

}