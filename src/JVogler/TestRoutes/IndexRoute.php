<?php
/**
 * Created by IntelliJ IDEA.
 * User: Jens
 * Date: 22.02.2018
 * Time: 13:53
 */

namespace JVogler\TestRoutes;


use FrameStack\Routing\RouteProviders\RouteProviders\Annotations\Annotations\Route;
use FrameStack\Routing\RouteProviders\RouteProviders\Annotations\Annotations\RouteGroup;
use Texedu\App\App;
use Texedu\App\Request;
use Texedu\App\Response;

/**
 * Class IndexRoute
 * @package JVogler\TestRoutes
 * @RouteGroup(url="/main")
 */
class IndexRoute
{

    /**
     * @Route(method="GET", url="/")
     */
    public function index(App $app, Request $request, Response $response) {

        return $response->text(file_get_contents(__DIR__ . '/test.html'));

    }

    /**
     * @Route(method="GET", url="/hello/{name}")
     */
    public function helloUser($params) {
        echo "Hello " . $params["name"];
    }

    /**
     * @Route(method="GET", url="/add/{a:\d+}/{b:\d+}")
     */
    public function adder($params) {
        echo $params['a'] + $params['b'];
    }

    /**
     * @Route(method="GET", url="/modul/{modulname}")
     */
    public function showModul($params) {
        $modulName = $params['modulname'];

        // DB modulbeschreibung holen



    }

}